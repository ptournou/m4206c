# Installation et prise en main des outils necessaires à la programmation Java et l'execution des programmes.

Référence pédagogique : https://www.emse.fr/~picard/cours/1A/java/livretJava.pdf
Note sur les classes et méthodes génériques : https://www.baeldung.com/java-generics


## Java Development Kit (JDK)

Télécharger et installer la JDK : https://www.oracle.com/technetwork/java/javase/downloads/index.html


Pour tester votre installation, creez un répertoire TDs (`mkdir TDs`), deplacez vous à l'interieur et créez un fichier src/HelloWord.java contenant la classe executable ci-dessous :

```java
public class HelloWorld {
    public static void main(String[] args) {
        System.out.println("\nHello World affiché par la fonction main de la classe executable HelloWorld. \nVivement la suite.\n");
    }
}
```

Compilez le fichier en spécifiant un répertoire dans lequel le resultat sera stocké :

`javac -d bin -c classpath src src/HelloWorld.java`

Executez votre code :

`java -classpath ./bin HelloWorld`

Installer un IDE (Ecplise) - (optionnel)

https://www.eclipse.org/downloads/packages/release/2019-03/r/eclipse-ide-java-developers

Suivre le cours et faire le TD java.