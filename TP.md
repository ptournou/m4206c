Chacun des exercices ci-dessous est à rendre et sera noté. A chaque fois qu'un exercice sera fini vous devrez l'archiver puis l'envoyer à pierre.tournoux@univ-reunion.fr. Pour génerer l'archive, utilisez la fonctionalité d'export d'Android Studio : File > Export to ZIP file.

Les points seront attribués en fonction :
* du nombre d'exercices réalisés
* des consignes respectées
* du caractère fonctionnel de l'application.

Des points seront retirés si :
* l'indendation est incorecte ou non uniforme
* les conventions de nommages pour les variables, methodes, classes (...) fournies dans le support de cours ne sont pas respectées
* si des copiers coller sont constatés

Aucun point ne vous sera retiré pour le caractère esthétique et érgonomique de votre application. Ce qui importe c'est que les éléments soient visibles et que l'application puisse être utilisée.

## TP1 - Plus ou moins

Créez une application comportant trois éléments visuels :
* Une zone de texte (`TextView`)
* Un champ de saisie de texte (`EditText`)
* Un bouton 

Lorsque l'application démarre, un nombre aléatoire entre 1 et 100 est généré. Le but de l'utilisateur est de trouver ce nombre avec le moins d'éssais possibles. Le champ texte sert à saisir le nombre, le bouton à valider et évaluer la saisie. Le `TextView` affichera si le nombre saisie est égale, plus grand ou plus petit que le nombre à trouver.

Pour générer un nombre aléatoire, vous pourrez utiliser Random :
````
Random pmRand = new Random();
int n = rand.nextInt(10)+1;
````

## TP2 - Jeu du pendu

Créez une application permettant de jouer au jeu du pendu. L'interface comprendra au moins trois éléments :
* Une zone de texte comportant les caractères devinés, ou un underscore (_) pour ceux qui n'ont pas encore été trouvés - (
* Une image preprésentant un personnage à pendre (`ImageView`)
* Une zone de texte pour le nombre de coups restant 
* Une zone de texte pour saisir le cacractère à deviner
* Un bouton permettant de valider la saisie du caractère

Vous ferez en sorte de pas permettre à l'utilisateur de saisir plusieurs fois le même caractère dans la même partie.
Au démarrage d'une partie, vous selectionnerez un mot parmis une liste fixe, par exemple :

````
Lys
Voie
Rue
Savate
Panneau
Chat
Lune
Canard
Turquoise
Trouble
Programmation
````
 
Les règles du pendu sont celle de Wikipedia en 10 coups : https://fr.wikipedia.org/wiki/Le_Pendu_(jeu)

Pour prendre en main les `ImageView`, vous pourrez revoir le tutoriel :
https://codelabs.developers.google.com/codelabs/android-training-clickable-images/index.html?index=..%2F..%2Fandroid-training#2

### Améliorations possibles :
* Après chaque coup joué par l'utilisateur, vous pourrez mettre à jour l'image du pendu. Vous pourrez utilisez les images `png` de la page Wikipedia.
Pour mettre à jour l'image d'une `ImageView`, vous pouvez utilisez la fonction : `imagePendu.setImageResource(R.drawable.android3d)`

### Notes :
N'hésitez pas à utiliser les LOG : https://codelabs.developers.google.com/codelabs/android-training-create-an-activity/index.html?index=..%2F..%2Fandroid-training#2 ainsi que le
debugger : https://codelabs.developers.google.com/codelabs/android-training-using-debugger/index.html.

La fonction qui construit le mot à afficher est un bon exemple pour utiliser les tests unitaire : https://codelabs.developers.google.com/codelabs/android-training-unit-tests/index.html


## TP 3 - Géolocalisation

Vous créerez une application permettant de suivre les coordonnées GPS d'un téléphone en les affichant à l'écran.

Vous implémenterez l'interface `LocationListener` sur votre `Activity` et récuperez une instance de LocationManager via :

`location_manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);`

puis demanderez des mises à jour sur vos coordonnées GPS via :

`location_manager.requestLocationUpdates(LocationManager.GPS_PROVIDER,3000,10,this);`

Vous afficherez à l'écran, dans un ou plusieurs `TextView`, les valeurs de latitude, longitute, vitesse, altitude et timestamp de votre téléphone récupérés de l'instance `Location` de la méthode `onLocationChanged` que vous aurez implémenté.

Pour vous familiariser avec le LOCATION_SERVICE, vous pourrez suivre le tutoriel : https://codelabs.developers.google.com/codelabs/advanced-android-training-device-location/index.html


### Améliorations possibles :

Vous pouvez afficher votre position sur une carte. Pour vous familiariser avec les maps et API google : https://codelabs.developers.google.com/codelabs/advanced-android-training-google-maps/index.html

## TP 4 - Webview

Dans cette application vous utiliserez les Webviews pour afficher du contenu HTML. Vous créerez une interface contenant trois éléments :
* Une Webview
* Un champ texte
* Un bouton

Lorsque le bouton est pressé, vous lancerez le téléchargement de l'URL fournie dans le champ texte et vous afficherez la page résultant de ce téléchargement dans la Webview. Vous pourrez vous aider de la documentation officielle d'Android : https://developer.android.com/guide/webapps/webview#java .

### Améliorations possibles :

Le téléchargement de la page HTML pourra être géré par votre code plutôt que par la Webview. Vous devrez ouvrir une socket, effectuer la requète HTTP, stocker le résultat dans une chaine puis l'afficher avec la Webview.

Vous pourrez ensuite interepter les téléchargements de liens HTTP provoqués par les clicks dans la WebView. Vous téléchargement le contenu avant de l'afficher à nouveau dans la WebView.