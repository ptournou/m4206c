Installation et prise en main des outils de developpement sous Android

## Téléchargez et installez Android Studio :
https://developer.android.com/studio

Suivez le chapitre "Android fundamentals 01.1: Android Studio and Hello World", cours en ligne de google codelabs :

https://developer.android.com/courses/fundamentals-training/toc-v2

Puis les chapitres suivant :
*  1.2-A : Première interface interactive
*  1.2-B : L'éditeur de mise en forme (layout)
*  2.1 : Les activités et les intents
*  2.2 : Cycle de vie d'une activité et états
*  2.3 : Intents implicites
*  3.1 : Le debugger
*  3.2 : Tests unitaires
*  7.1 : AsyncTask
*  7.2 : Asyntask et Aynctask Loader
*  7.3 : Broadcast Receivers
*  8.2 : Alarm Manager
*  8.3 : Job Scheduler